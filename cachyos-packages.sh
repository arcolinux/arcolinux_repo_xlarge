#!/bin/bash
#CHROOT=$HOME/Documents/chroot-archlinux
#https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_Clean_Chroot
#https://archlinux.org/news/git-migration-completed/
#https://wiki.archlinux.org/title/DeveloperWiki:HOWTO_Be_A_Packager
##################################################################################################################
# Written to be used on 64 bits computers
# Author    :   Erik Dubois
# Website   :   http://www.erikdubois.be
##################################################################################################################
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#tput setaf 0 = black 
#tput setaf 1 = red 
#tput setaf 2 = green
#tput setaf 3 = yellow 
#tput setaf 4 = dark blue 
#tput setaf 5 = purple
#tput setaf 6 = cyan 
#tput setaf 7 = gray 
#tput setaf 8 = light blue

destination1="/var/cache/pacman/pkg"
destination2=$HOME"/ARCO/ARCOLINUX-REPO/arcolinux_repo_xlarge/x86_64/"

echo "toggle-arcolinux-repo"
/usr/local/bin/toggle-arcolinux-repo
echo "toggle-chaotic-repo"
/usr/local/bin/toggle-chaotic-repo

sudo pacman -Scc

sudo pacman -Sy


sudo pacman -Sw linux-cachyos linux-cachyos-headers

echo "New line - removing garuda signature"
sleep 5
rm $destination1/*.sig
cp -v $destination1/* $destination2

/usr/local/bin/toggle-chaotic-repo
/usr/local/bin/toggle-arcolinux-repo

tput setaf 11;
echo "#############################################################################################"
echo "###################                       build done                   ######################"
echo "#############################################################################################"
tput sgr0
  
